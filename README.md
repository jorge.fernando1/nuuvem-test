# Nuuvem web-1 test


## Description

This project have been created to accomplish the test that was passed
The project consists only a Rails project,


## Setup

To setup the application, follow the steps:
1. Check your Ruby version, the application uses the 2.5.3 version, if you don't have, update your Ruby or use some version manager as `rvm` or `rbenv`.
2. Use the file `.env.sample` as template, create the files `.env` and `.env.test` and configure the Environment Variables.
3. Run the command `rake db:create` in order to create the database.
4. Run the command `rake db:migrate` in order to create the database's schema.
5. Run the command `rails server` to up the web server.


## Running the tests

In the project's root path only type `rspec` in order to running the tests.
If the database haven't created, run the commands `RAILS_ENV=test rails db:create` and `RAILS_ENV=test rails db:migrate` to create it.


## Descrição

Este projeto foi criado para a realização do teste que foi passado para o cargo de desenvolvedor.
O projeto consiste apenas em um projeto Rails.


## Configuração

Para subir a aplicação, siga os seguintes passos:
1. Verifique sua instalação do ruby, o sistema utiliza o ruby 2.5.3, caso seja necessário atualize o Ruby ou utilize algum gerenciador de versão, como o `rvm` ou `rbenv`
2. Usando o arquivo `.env.sample` como template, crie os arquivos `.env` e `.env.test` e configure as variáveis de ambiente.
3. Execute o comando `rake db:create` para criar o banco de dados.
4. Execute o comando `rake db:migrate` para criar as tabelas do banco. 
5. Execute o comando `rails server` para executar o servidor web.

## Executando a suite de testes

Na pasta raiz do projeto simplesmente utilize o comando `rspec` para que os testes sejam executados.
Se o banco de dados não tiver sido criado, utilize os comandos `RAILS_ENV=test rails db:create` e `RAILS_ENV=test rails db:migrate` para criá-lo.
