module SalesParser
  module Parser
    class Line
      ##
      #
      # Parse a sale line, returning a hash containing the header key and its value
      # Raise an ArgumentError if the number of values in the line doesn't match
      # the number of headers keys.
      #
      # @param [Hash] headers - the hash returned by SalesParser::Parser::Header
      # @param [String] line - the line containing the sale's value.
      #
      # @return [Hash] containing the line values with the correct header.
      # Example:
      #   headers: { foo: 0, bar: 1 }
      #   line: "Nice foo\tSuper  Bar baz"
      #   return: { foo: 'Nice foo', bar: 'Super  Bar baz' }
      #
      def self.parse(headers:, line:)
        self.new(headers: headers, line: line).parse!
      end

      def initialize(headers:, line:)
        @headers = headers
        @line = parse_line(line)
        @response = {}
      end

      def parse!
        values = @line.split("\t")
        raise ArgumentError.new('The number values in line must the same number of headers') if values.size != @headers.size

        @headers.each do |key, index|
          @response[key] = values[index]
        end

        @response
      end

      private

      def parse_line(line)
        raise ArgumentError.new('Line can\'t be an empty string') if line.strip.empty?
        line.gsub("\n", '')
      end
    end
  end
end
