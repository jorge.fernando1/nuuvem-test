module SalesParser
  module Parser
    ##
    # This class is responsible to parse and normalize the headers, in order to
    # get the correct sale's data.
    #
    class Header
      ##
      # Parse and normalize the headers.
      # @param [String] line - The headers string (separeted by a tab "\t")
      #
      # @return [Hash] with the header value and your index.
      # The return follow the syntax { snaked_case_key: key_index }
      # For example:
      #
      #  +line = "some\theaders\ton the\tline"+
      #  +{ some: 0, headers: 1, on_the: 2, line: 3 }+
      #
      def self.parse(line)
        self.new(line).parse!
      end

      def initialize(line)
        @line = parse_line(line)
        @headers = {}
      end

      def parse!
        keys = @line.split("\t")
        keys.each_with_index do |key, index|
          k = snake_key(key)
          @headers[k] = index
        end
        @headers
      end

      private

      def parse_line(line)
        raise ArgumentError.new('Line param can\'t be nil') if (line.nil? || line.strip.empty?)
        line.gsub("\n", '')
      end

      def snake_key(key)
        key.split.join('_').downcase.to_sym
      end
    end
  end
end
