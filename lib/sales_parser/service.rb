require 'sales_parser/parser/header'
require 'sales_parser/parser/line'
require 'sales_parser/builder'

module SalesParser
  ##
  # Use this class to correct parse and save the sales in the uploaded files.
  class Service
    ##
    # Creates a new service instance to parse the uploaded file.
    #
    # @param [Upload] upload The uploaded file that need be parsed.
    def initialize(upload:)
      @upload = upload
      @file = upload.file
      @builder = SalesParser::Builder.new(upload: upload)
    end

    ##
    # Loaded the uploaded file, normalize the data and save the [Sale] sales.
    #
    # @return [Upload] the uploaded file with associated sales. 
    def parse_sales
      parse_file!

      ActiveRecord::Base.transaction do
        @lines.each { |line_data| @builder.build_sale(line_data) }
        @upload.tap(&:save!)
      end
    end

    private

    def parse_file!
      lines = File.open(@file.current_path).map { |line| line }
      parse_header(lines.shift)
      parse_lines(lines)
    end


    def parse_header(line)
      @headers = SalesParser::Parser::Header.parse(line)
    end

    def parse_lines(lines)
      @lines = lines.map { |line| SalesParser::Parser::Line.parse(headers: @headers, line: line) }
    end
  end
end
