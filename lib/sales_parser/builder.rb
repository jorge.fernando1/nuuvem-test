Dir[File.dirname(__FILE__) + '/builders/*.rb'].each { |file| require file }

module SalesParser
  ##
  # This class is responsible to build the sales for an upload, loading the hash
  # that has built using the +SalesParser::Parser::Line+ class and saving the normalized
  # data in the database.
  #
  class Builder
    attr_accessor :sales

    def initialize(upload:)
      @upload = upload
      @sales = []
    end

    ##
    #
    # Get the parsed line, normalize the data and save the sale in the database.
    # @param [Hash] sale_data - the parsed data using the +SalesParser::Parser::Line+ class
    #
    def build_sale(sale_data)
      build_data!(sale_data)

      sale = find_sale || create_sale
      SalesParser::Builders::ItemSale.build(sale: sale, item: @item, price: sale_data[:item_price].to_f, quantity: sale_data[:purchase_count])
      @sales.push(sale)
    end


    private


    def find_sale
      @sales.find { |sale| sale.client_id == @client.id }
    end

    def create_sale
      ::Sale.create(client: @client, upload: @upload)
    end

    def build_data!(sale_data)
      build_client(sale_data)
      build_merchant(sale_data)
      build_item(sale_data)
    end

    def build_client(sale_data)
      @client = SalesParser::Builders::Client.build(name: sale_data[:purchaser_name])
    end

    def build_merchant(sale_data)
      @merchant = SalesParser::Builders::Merchant.build(name: sale_data[:merchant_name], address: sale_data[:merchant_address])
    end

    def build_item(sale_data)
      @item = SalesParser::Builders::Item.build(description: sale_data[:item_description], price: sale_data[:item_price], merchant: @merchant)
    end
  end
end
