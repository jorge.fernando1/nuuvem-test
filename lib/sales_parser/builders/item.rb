module SalesParser
  module Builders
    ##
    #
    # This class is responsible to build an Item object, creating a new or finding
    # in the database if the record already exists.
    class Item
      ##
      # The method lookup the database to finding if the Item already added.
      # If found an Item with the same description for the same merchant, update the price
      # and return the record. If not found any Item, create the item with the data.
      #
      # @param [String] description - the item description
      # @param [Float] price - the item price
      # @páram [Merchant] merchant - the merchant object that found by SalesParser::Builder
      #
      # @return [Item] the found or created item record. 
      #
      def self.build(description:, price:, merchant:)
        ::Item.find_or_create_by!(description: description, merchant_id: merchant.id).tap do |item|
          item.price = price
          item.save!
        end
      end
    end
  end
end
