module SalesParser
  module Builders
    ##
    #
    # This class is responsible to build a Merchant object, creating a new or finding
    # in the database if the record already exists.
    class Merchant
      ##
      # The method lookup the database if a merchant with the name and address already existis, if nothing
      # is found, create the a new merchant with the data.
      #
      # @param [String] name - the merchant name
      # @param [String] address - the merchant address
      # @return [Merchant] - the Merchant object.
      #
      def self.build(name:, address:)
        ::Merchant.find_or_create_by(name: name, address: address)
      end
    end
  end
end
