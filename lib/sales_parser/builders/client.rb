module SalesParser
  module Builders
    ##
    #
    # This class is responsible to build a Client object, creating a new or finding
    # in the database if the client record already exists.
    class Client
      ##
      # The method lookup the database if a client with the name already existis, if nothing
      # is found, create the new client with the following data.
      #
      # @param [String] name - the client name
      # @return [Client] - the Client object.
      #
      def self.build(name:)
        ::Client.find_or_create_by!(name: name)
      end
    end
  end
end
