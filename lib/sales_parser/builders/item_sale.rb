module SalesParser
  module Builders
    ##
    #
    # This class is responsible to build an ItemSale object, creating a new or finding
    # in the database if the record already exists.
    #
    class ItemSale

      ##
      # The method lookup the database if for the sale, a same item with the
      # same price already added. If does, just add the quantity in the record
      # and return it. If not, create a new ItemSale register.
      #
      # @param [Sale] sale - The sale that has been built.
      # @param [Item] item - The item that has sold.
      # @param [Float] price - The item price.
      # @param [Integer] quantity - Number of items sold.
      #
      # @return [ItemSale] - the ItemSale for the Sale. 
      #
      def self.build(sale:, item:, price:, quantity:)
        new(sale, item, price, quantity).build!
      end

      def initialize(sale, item, price, quantity)
        @sale = sale
        @item = item
        @price = Money.from_amount(price)
        @quantity = quantity
        @item_sale = @sale.item_sales.find_by_item_id(@item.id)
      end

      def build!
        return new_item_sale unless @item_sale
        @item_sale.price == @price ? update_item_sale : new_item_sale
      end

      private

      def new_item_sale
        item_sale = @sale.item_sales.build(price: @price, quantity: @quantity, item: @item)
        item_sale.tap(&:save!)
      end

      def update_item_sale
        @item_sale.tap do |item_sale|
          item_sale.quantity += @quantity
          item_sale.save!
        end
      end
    end
  end
end
