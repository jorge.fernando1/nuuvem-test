Rails.application.routes.draw do
  devise_for :users, skip: [:passwords]
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  resources :uploads, except: [:update, :edit, :destroy]

  root to: 'home#index'
end
