FactoryBot.define do
  factory :user do
    sequence(:name) { |i|  "User number #{i}" }
    sequence(:email) { |i| "user_#{i}@mail.com" }
    password { '123456789' }
  end
end
