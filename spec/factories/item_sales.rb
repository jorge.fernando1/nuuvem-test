FactoryBot.define do
  factory :item_sale do
    price { Money.from_amount(rand * 100) }
    quantity { rand(10) + 1 }
    sale
    item
  end
end
