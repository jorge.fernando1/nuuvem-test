FactoryBot.define do
  factory :merchant do
    name { 'Super company' }
    address { '123 Company St.' }
  end
end
