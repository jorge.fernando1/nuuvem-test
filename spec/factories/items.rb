FactoryBot.define do
  factory :item do
    description { 'Item lorem ipsum' }
    price { Money.from_amount(rand * 100) }
    merchant
  end
end
