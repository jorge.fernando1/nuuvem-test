FactoryBot.define do
  factory :upload do
    sequence(:description) { |i| "Some text description to upload #{i}" }
    file { Rack::Test::UploadedFile.new(Rails.root.join("spec/support/tab_files/#{file_name}"), 'text/plain') }
    user

    transient do
      file_name { 'simple_sale.tab' }
    end
  end
end
