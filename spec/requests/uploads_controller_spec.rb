require 'rails_helper'
include ActionDispatch::TestProcess

describe UploadsController do
  let(:user) { create(:user) }
  before(:each) { sign_in(user) }

  describe 'GET /uploads/new' do
    before(:each) do
      get new_upload_path
    end

    it 'return HTTP 200 status' do
      expect(response.status).to eql 200
    end

    it 'render :new template' do
      expect(response).to render_template(:new)
    end
  end

  describe 'POST /uploads' do
    before(:each) do
      post uploads_path, params: { upload: { description: 'upload description', file: file } }
    end

    context 'when uploaded file contains error' do
      let(:file) { fixture_file_upload('spec/support/tab_files/error_sale.tab') }

      it 'render :new template' do
        expect(response).to render_template(:new)
      end

      it 'dont save anything' do
        expect(Upload.count).to eql 0
        expect(Client.count).to eql 0
        expect(Sale.count).to eql 0
        expect(Merchant.count).to eql 0
        expect(Item.count).to eql 0
      end
    end

    context 'when everything goes well' do
      let(:file) { fixture_file_upload('spec/support/tab_files/simple_sale.tab') }

      it 'redirect to show' do
        expect(response).to redirect_to upload_path(assigns(:upload))
      end

      it 'create the sales data' do
        expect(Upload.count).to eql 1
        expect(Client.count).to eql 1
        expect(Sale.count).to eql 1
        expect(Merchant.count).to eql 1
        expect(Item.count).to eql 1
      end
    end
  end

end
