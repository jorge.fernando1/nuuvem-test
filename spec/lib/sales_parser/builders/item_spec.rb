require 'rails_helper'
require 'sales_parser/builders/item'

shared_examples_for 'creates a new item' do
  let(:options) { {} }

  it 'create a new Item' do
    item = described_class.build(description: options[:description], price: options[:price], merchant: options[:merchant])

    expect(Item.count).to eql 2
    expect(item.id).to_not eql item_merchant.id
  end
end

describe SalesParser::Builders::Item do
  describe '#build' do
    let(:item_merchant) { create(:merchant) }
    let(:item_price) { 1.0 }
    let(:item_description) { 'Lorem ipsum!' }
    let!(:created_item) { create(:item, description: item_description, price: item_price, merchant: item_merchant) }

    let(:other_merchant) { create(:merchant) }
    let(:other_price) { 15.0 }
    let(:other_description) { 'Ipsum lorem lorem' }


    context 'when description=equal, price=equal, merchant=different' do
      it_behaves_like 'creates a new item' do
        let(:options) { { description: item_description, price: item_price, merchant: other_merchant } }
      end
    end

    context 'when description=equal, price=different, merchant=different' do
      it_behaves_like 'creates a new item' do
        let(:options) { { description: item_description, price: other_price, merchant: other_merchant } }
      end
    end

    context 'when description=different, price=equal, merchant=equal' do
      it_behaves_like 'creates a new item' do
        let(:options) { { description: other_description, price: item_price, merchant: item_merchant } }
      end
    end

    context 'when description=different, price=equal, merchant=different' do
      it_behaves_like 'creates a new item' do
        let(:options) { { description: other_description, price: item_price, merchant: other_merchant } }
      end
    end

    context 'when description=different, price=different, merchant=equal' do
      it_behaves_like 'creates a new item' do
        let(:options) { { description: other_description, price: other_price, merchant: item_merchant } }
      end
    end

    context 'when description=different, price=different, merchant=different' do
      it_behaves_like 'creates a new item' do
        let(:options) { { description: other_description, price: other_price, merchant: other_merchant } }
      end
    end


    context 'return the already created item' do
      it 'when description=equal, price=equal, merchant=equal' do
        item = described_class.build(description: item_description, price: item_price, merchant: item_merchant)
        expect(Item.count).to eql 1
        expect(item.id).to eql created_item.id
      end
    end

    context 'update the already created item' do
      it 'when description=equal, price=different, merchant=equal' do
        item = described_class.build(description: item_description, price: other_price, merchant: item_merchant)
        expect(Item.count).to eql 1
        expect(item.id).to eql created_item.id
        expect(item.price).to eql Money.from_amount(other_price)
      end
    end



  end
end
