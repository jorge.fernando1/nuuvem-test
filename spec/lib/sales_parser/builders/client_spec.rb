require 'rails_helper'
require 'sales_parser/builders/client'

describe SalesParser::Builders::Client do
  describe '#build' do
    context 'when is a new client' do
      before(:each) { @client = described_class.build(name: 'John John') }

      it 'create a new register' do
        expect(Client.count).to eql 1
      end

      it 'expect new client has the correct info' do
        expect(@client.name).to eql 'John John'
      end
    end

    context 'when the client already exists' do
      let!(:client) { create(:client, name: 'John John') }
      before(:each) { @client = described_class.build(name: 'John John') }

      it 'dont create a new register' do
        expect(Client.count).to eql 1
      end

      it 'return the client' do
        expect(@client.id).to eql client.id
        expect(@client.name).to eql client.name
      end
    end

  end
end
