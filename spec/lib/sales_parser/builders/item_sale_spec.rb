require 'rails_helper'
require 'sales_parser/builders/item_sale'

describe SalesParser::Builders::ItemSale do
  describe '#build' do
    let(:sale) { create(:sale) }
    let(:item) { create(:item) }

    context 'when is a new item in sale' do
      it 'create a new record' do
        item_sale = described_class.build(sale: sale, item: item, price: 100.0, quantity: 3)
        expect(ItemSale.count).to eql 1

        expect(item_sale.sale).to eql sale
        expect(item_sale.price).to eql Money.from_amount(100.0)
        expect(item_sale.quantity).to eql 3
      end
    end

    context 'when already exist an item in sale' do
      let!(:previous_item_sale) { create(:item_sale, sale: sale, item: item, price: 200, quantity: 2) }

      context 'but the price is different' do
        it 'create a new sale_item in sale' do
          item_sale = described_class.build(sale: sale, item: item, price: 100.0, quantity: 2)
          sale.reload

          expect(ItemSale.count).to eql 2
          expect(sale.item_sales.size).to eql 2
          expect(sale.item_sales).to include(item_sale)
          expect(sale.item_sales).to include(previous_item_sale)
        end
      end

      context 'and the price is the same' do
        it 'sum the quantity in the previous sale_item', :test do
          item_sale = described_class.build(sale: sale, item: item, price: 200.0, quantity: 3)
          sale.reload

          expect(ItemSale.count).to eql 1
          expect(sale.item_sales.size).to eql 1
          expect(item_sale.id).to eql previous_item_sale.id
          expect(item_sale.quantity).to eql 5 # 2 + 3
        end
      end
    end

  end
end
