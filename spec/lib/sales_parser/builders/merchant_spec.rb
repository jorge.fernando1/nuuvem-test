require 'rails_helper'
require 'sales_parser/builders/merchant'

describe SalesParser::Builders::Merchant do
  describe '#build' do
    context 'when is a new merchant' do
      before(:each) { @merchant = described_class.build(name: 'Foo', address: '123, Baz avenue') }

      it 'create a new record' do
        expect(Merchant.count).to eql 1
      end

      it 'return the created merchant' do
        expect(@merchant.name).to eql 'Foo'
      end
    end

    context 'when the merchant already exists' do
      let!(:merchant) { create(:merchant, name: 'Foo', address: '123, Baz avenue') }
      before(:each) { @merchant = described_class.build(name: 'Foo', address: '123, Baz avenue') }
      
      it 'dont create a new record' do
        expect(Merchant.count).to eql 1
      end

      it 'return the already created merchant' do
        expect(@merchant.name).to eql 'Foo'
      end
    end
  end
end
