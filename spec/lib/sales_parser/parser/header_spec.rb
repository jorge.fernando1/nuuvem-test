require 'rails_helper'
require 'sales_parser/parser/header'

describe SalesParser::Parser::Header do
  describe '#parse' do
    context 'when some goes wrong' do
      context 'when line param is nil' do
        it 'raise an ArgumentError exception' do
          expect {
            described_class.parse(nil)
          }.to raise_error(ArgumentError, 'Line param can\'t be nil')
        end
      end
      context 'when line param is an empty string' do
        it 'raise an ArgumentError exception' do
          expect {
            described_class.parse('   ')
          }.to raise_error(ArgumentError, 'Line param can\'t be nil')
        end
      end
    end

    context 'when everything goes well' do
      let(:line) { "purchaser name\titem description\titem price\tpurchase count\tmerchant address\tmerchant name\n" }
      subject { described_class.parse(line) }

      describe '@line' do
        it 'expect remove the break line "\n"' do
          parser = described_class.new("line\twith\tbreak\tline\n")
          @line = parser.instance_variable_get('@line')

          expect(@line.include?("\n")).to be false
        end
      end

      describe ':return' do
        it 'return a hash' do
          expect(subject.class).to eql Hash
        end

        describe '- keys' do
          it 'return all keys in snake_case' do
            expect(subject).to be_has_key(:purchaser_name)
            expect(subject).to be_has_key(:item_description)
            expect(subject).to be_has_key(:item_price)
            expect(subject).to be_has_key(:purchase_count)
            expect(subject).to be_has_key(:merchant_address)
            expect(subject).to be_has_key(:merchant_name)
          end

          context 'when header include various spaces between words' do
            let(:line) { "purchaser   name\titem  description" }

            it 'remove the spaces and parse to snake_case' do
              expect(subject).to be_has_key(:purchaser_name)
              expect(subject).to be_has_key(:item_description)
            end
          end

          context 'when header include capital letters' do
            let(:line) { "purchAser Name\tItem Description" }

            it 'downcase all letters' do
              expect(subject).to be_has_key(:purchaser_name)
              expect(subject).to be_has_key(:item_description)
            end
          end
        end

        describe ' - values' do
          it 'contains the index of the header' do
            expect( subject[:purchaser_name] ).to eql 0
            expect( subject[:item_description]).to eql 1
            expect( subject[:item_price] ).to eql 2
            expect( subject[:purchase_count] ).to eql 3
            expect( subject[:merchant_address] ).to eql 4
            expect( subject[:merchant_name] ).to eql 5
          end
        end
      end
    end
  end
end
