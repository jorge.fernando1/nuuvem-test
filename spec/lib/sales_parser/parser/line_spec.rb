require 'rails_helper'
require 'sales_parser/parser/line'

describe SalesParser::Parser::Line do
  describe '#parse' do
    let(:header) { { client_name: 0, item_description: 1, quantity: 2 } }
    let(:line) { "John Doe\tSnack bars\t10\n" }

    subject { described_class.parse(headers: header, line: line) }

    context 'when something goes wrong' do
      context 'when line is an empty string' do
        let(:line) { "   " }

        it 'raise an Argument error' do
          expect {
            described_class.parse(headers: header, line: line)
          }.to raise_error(ArgumentError)
        end
      end
      context 'when the line doesn\'t have the correct number of values' do
        let(:line) { "John Doe\tSnack bars\n" }

        it 'raise an ArgumentError' do
          expect {
            described_class.parse(headers: header, line: line)
          }.to raise_error(ArgumentError, 'The number values in line must the same number of headers')
        end
      end
    end

    context 'when everything goes well' do
      describe ': @line' do
        it 'remove the break line "\n"' do
          parser = described_class.new(headers: header, line: line)
          @line = parser.instance_variable_get('@line')
          expect(@line.include?("\n")).to be false
        end
      end

      describe ': returns' do
        it 'a hash' do
          expect(subject.class).to eql Hash
        end

        it 'the hash must contain all headers as keys' do
          expect(subject.keys.size).to eql 3
          expect(subject).to be_has_key(:client_name)
          expect(subject).to be_has_key(:item_description)
          expect(subject).to be_has_key(:quantity)
        end

        it 'each key must have the correct value' do
          expect( subject[:client_name] ).to eql 'John Doe'
          expect( subject[:item_description] ).to eql 'Snack bars'
          expect( subject[:quantity] ).to eql '10'
        end
      end
    end
  end
end
