require 'rails_helper'
require 'sales_parser/builder'

def build_sales_data(n, options = {})
  n.times.map do |i|
    {
      purchaser_name: "Client #{i}",
      item_description: "Item #{i}",
      item_price: (rand(i) + 1).to_i * 5,
      purchase_count: (rand(i) + 1).to_i,
      merchant_address: "Address #{i}",
      merchant_name: "Merchant #{i}"
    }.merge(options)
  end
end

describe SalesParser::Builder do
  describe '#build_sale' do
    let(:upload) { create(:upload) }

    subject { described_class.new(upload: upload) }

    context 'when receive only one sale from one client' do
      before(:each) do
        sales_data = build_sales_data(1, purchaser_name: 'John Doe')
        sales_data.each { |line| subject.build_sale(line) }
        upload.reload
      end

      let(:sale) { upload.sales.first }

      it 'create 1 sale' do
        expect(upload.sales.count).to eql 1
      end

      it 'create 1 client' do
        expect(Client.count).to eql 1
        expect(sale.client).to_not be_nil
      end

      it 'create 1 item' do
        expect(Item.count).to eql 1
        expect(sale.items.count).to eql 1
      end

      it 'create 1 item_sale' do
        expect(ItemSale.count).to eql 1
        expect(sale.item_sales.count).to eql 1
      end
    end

    context 'when receive one sale with two items' do
      before(:each) do
        data = build_sales_data(2, purchaser_name: 'Marty')
        data.each { |line| subject.build_sale(line) }
        upload.reload
      end

      it 'create one client' do
        expect(Client.count).to eql 1
      end

      it 'create one sale' do
        expect(Sale.count).to eql 1
      end

      it 'create two items' do
        expect(Item.count).to eql 2
      end

      it 'create two item_sales' do
        expect(ItemSale.count).to eql 2
      end

    end

    context 'when receive two sales with one item each' do
      before(:each) do
        data = build_sales_data(2)
        data.each { |line| subject.build_sale(line) }
        upload.reload
      end

      it 'create two clients' do
        expect(Client.count).to eql(2)
      end
      it 'create two sales' do
        expect(Sale.count).to eql 2
        expect(upload.sales.count).to eql 2
      end
      it 'create two items' do
        expect(Item.count).to eql 2
      end
      it 'create two item_sales' do
        expect(ItemSale.count).to eql 2
        upload.sales.each { |sale| expect(sale.item_sales.size).to eql(1) }
      end
    end

    context 'when receive two sales with the same item' do
      before(:each) do
        data = build_sales_data(2, item_description: 'nice item', item_price: 5, merchant_name: 'MM', merchant_address: 'Street')
        data.each { |line| subject.build_sale(line) }
        upload.reload
      end

      it 'create two clients' do
        expect(Client.count).to eql 2
      end
      it 'create two sales' do
        expect(Sale.count).to eql 2
      end
      it 'create one item' do
        expect(Item.count).to eql 1
        item = Item.first
        upload.sales.each do |sale|
          expect(sale.items.first).to eql item
        end
      end

      it 'create two item_sales' do
        expect(ItemSale.count).to eql 2
      end
    end

    context 'when receive two sales with the same two items' do
      before(:each) do
        data_1 = build_sales_data(2, item_description: 'nice item', item_price: 5, merchant_name: 'MM', merchant_address: 'Street')
        data_2 = build_sales_data(2, item_description: 'other', item_price: 7, merchant_name: 'BBBB', merchant_address: 'Avenue main')
        (data_1 + data_2).each { |line| subject.build_sale(line) }
        upload.reload
      end
      it 'create 2 client' do
        expect(Client.count).to eql 2
      end
      it 'create 2 sales' do
        expect(Sale.count).to eql 2
        expect(upload.sales.size).to eql 2
      end

      it 'create 2 items' do
        expect(Item.count).to eql 2
      end

      it 'create 4 item sales' do
        expect(ItemSale.count).to eql 4
      end
    end

    context 'when receive two sales with items differents' do
      before(:each) do
        d1 = build_sales_data(1, purchaser_name: 'client_A', item_description: 'Item A')
        d2 = build_sales_data(1, purchaser_name: 'client_A', item_description: 'Item B')
        d3 = build_sales_data(1, purchaser_name: 'client_B', item_description: 'Item C')
        d4 = build_sales_data(1, purchaser_name: 'client_B', item_description: 'Item D')

        (d1 + d2 + d3 + d4).each { |line| subject.build_sale(line) }
        upload.reload
      end
      it 'create 2 clients' do
        expect(Client.count).to eql 2
      end

      it 'create 2 sales' do
        expect(Sale.count).to eql 2
        expect(upload.sales.size).to eql 2
      end

      it 'create 4 items' do
        expect(Item.count).to eql 4
        upload.sales.each { |sale| expect(sale.items.size).to eql 2 }
      end

      it 'create 4 item sales' do
        expect(Item.count).to eql 4
        upload.sales.each { |sale| expect(sale.item_sales.size).to eql 2 }
      end
    end
  end
end
