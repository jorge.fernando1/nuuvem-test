require 'rails_helper'
require 'sales_parser/service'

#purchaser name	item description	item price	purchase count	merchant address	merchant name
#John Doe  Snack bars  10.0  2 123, Fake Avenue  Fake Store

describe SalesParser::Service do
  describe '#parse_sales' do
    subject { described_class.new(upload: upload) }
    before(:each) do
      subject.parse_sales
      upload.reload
    end

    context 'when contains one sale,' do
      context 'one item' do
        context 'and one merchant' do
          let(:upload) { create(:upload, file_name: '1sale_1item_1merchant.tab') }
          it 'create 1 sale' do
            expect(Sale.count).to eql 1
            expect(upload.sales.count).to eql 1
          end

          it 'create 1 item' do
            expect(Item.count).to eql 1
            expect(upload.sales.first.items.count).to eql 1
          end

          it 'create i merchant' do
            expect(Merchant.count).to eql 1
          end
        end
      end

      context 'two items' do
        context 'and one merchant' do
          let(:upload) { create(:upload, file_name: '1sale_2item_1merchant.tab') }
          it 'create 1 sale' do
            expect(Sale.count).to eql 1
            expect(upload.sales.count).to eql 1
          end

          it 'create 2 items' do
            expect(Item.count).to eql 2
            expect(upload.sales.first.items.count).to eql 2
          end

          it 'create 1 merchant' do
            expect(Merchant.count).to eql 1
          end
        end

        context 'and two merchants' do
          let(:upload) { create(:upload, file_name: '1sale_2item_2merchant.tab') }

          it 'create 1 sale' do
            expect(Sale.count).to eql 1
            expect(upload.sales.count).to eql 1
          end

          it 'create 2 items' do
            expect(Item.count).to eql 2
            expect(upload.sales.first.items.count).to eql 2
          end

          it 'create 2 merchants' do
            expect(Merchant.count).to eql 2
          end
        end
      end
    end

    context 'when contains two sales,' do
      context 'one item' do
        context 'and one merchant' do
          let(:upload) { create(:upload, file_name: '2sale_1item_1merchant.tab') }

          it 'create 2 sales' do
            expect(Sale.count).to eql 2
            expect(upload.sales.size).to eql 2
          end

          it 'create 2 clients' do
            expect(Client.count).to eql 2
          end

          it 'create 1 item' do
            expect(Item.count).to eql 1
          end

          it 'create 1 merchant' do
            expect(Merchant.count).to eql 1
          end
        end
      end

      context 'two items' do
        context 'and one merchant' do
          let(:upload) { create(:upload, file_name: '2sale_2item_1merchant.tab') }

          it 'create 2 sales' do
            expect(Sale.count).to eql 2
            expect(upload.sales.size).to eql 2
          end

          it 'create 2 clients' do
            expect(Client.count).to eql 2
          end

          it 'create 2 item' do
            expect(Item.count).to eql 2
          end

          it 'create 1 merchant' do
            expect(Merchant.count).to eql 1
          end
        end

        context 'and two merchants' do
          let(:upload) { create(:upload, file_name: '2sale_2item_2merchant.tab') }

          it 'create 2 sales' do
            expect(Sale.count).to eql 2
            expect(upload.sales.size).to eql 2
          end

          it 'create 2 clients' do
            expect(Client.count).to eql 2
          end

          it 'create 2 items' do
            expect(Item.count).to eql 4
          end

          it 'create 2 item_sales for each sale' do
            expect(ItemSale.count).to eql 4
            upload.sales.each do |sale|
              expect(sale.item_sales.size).to eql 2
            end
          end

          it 'create 2 merchants' do
            expect(Merchant.count).to eql 2
          end
        end
      end
    end

    context 'when items in file has in a different order' do
      let(:upload) { create(:upload, file_name: 'different_order.tab')}

      it 'create the client' do
        expect(Client.count).to eql 1

        client = Client.first
        expect(client.name).to eql 'John Doe'
      end

      it 'create the merchant' do
        expect(Merchant.count).to eql 1

        merchant = Merchant.first
        expect(merchant.name).to eql 'Fake Store'
        expect(merchant.address).to eql '123, Fake Avenue'
      end

      it 'create the item' do
        expect(Item.count).to eql 1
        item = Item.first
        expect(item.description).to eql 'Snack bars'
        expect(item.price).to eql Money.from_amount(10.0)
      end
    end
  end
end
