require 'rails_helper'

feature 'Only show accepts logged users' do
  let(:user) { create(:user, password: '12345678') }

  scenario 'Don\'t permit not logged users' do
    visit '/'
    expect(page).to have_content('You need to sign in or sign up before continuing.')
  end

  scenario 'Show home page to a logged user' do
    login_as(user, scope: :user)
    
    visit '/'
    expect(page).to have_content("Welcome #{user.name}!")
  end
end
