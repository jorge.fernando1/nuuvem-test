require 'rails_helper'

feature 'Upload a new sales file' do
  let(:user) { create(:user) }

  scenario 'Upload a file with error' do
    login_as(user, scope: :user)
    visit '/uploads/new'

    within('form') do
      fill_in 'Description', with: 'Capybara wrong file'
      attach_file('File', Rails.root.join('spec/support/tab_files/error_sale.tab'))
      click_button 'Salvar'
    end

    expect(page).to have_content('Error during importing sales, please check the file')
    expect(find_field('Description').value).to eql 'Capybara wrong file'
  end

  scenario 'Upload a correct file' do
    login_as(user, scope: :user)
    visit '/uploads/new'

    within('form') do
      fill_in 'Description', with: 'Capybara Correct File'
      attach_file('File', Rails.root.join('spec/support/tab_files/simple_sale.tab'))
      click_button 'Salvar'
    end

    expect(page).to have_content 'Upload: Capybara Correct File'
    expect(page).to have_content 'John Doe'
    expect(page).to have_content 'Total gross'
    expect(page).to have_content '10.0'
  end
end
