class CreateItemSales < ActiveRecord::Migration[5.2]
  def change
    create_table :item_sales, primary_key: %i[item_id sale_id] do |t|
      t.references :item, foreign_key: true
      t.references :sale, foreign_key: true
      t.integer :quantity
      t.decimal :price, precision: 7, scale: 2

      t.timestamps
    end
  end
end
