class ChangePrimaryForItemSalesToId < ActiveRecord::Migration[5.2]
  def change
    execute <<-SQL
      ALTER TABLE item_sales DROP CONSTRAINT item_sales_pkey
    SQL

    add_column :item_sales, :id, :primary_key
  end
end
