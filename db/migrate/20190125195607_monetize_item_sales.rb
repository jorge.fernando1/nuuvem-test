class MonetizeItemSales < ActiveRecord::Migration[5.2]
  def change
    remove_column :item_sales, :price
    add_monetize :item_sales, :price
  end
end
