class RemoveMerchantIdFromSales < ActiveRecord::Migration[5.2]
  def change
    remove_column :sales, :merchant_id
  end
end
