require 'sales_parser/service'

class UploadsController < ApplicationController
  def index
    @uploads = current_user.uploads
  end

  def new
    @upload = Upload.new
  end

  def create
    begin
      @upload = Upload.create(upload_params.merge(user: current_user))
      @service = SalesParser::Service.new(upload: @upload)
      @service.parse_sales

      redirect_to @upload
    rescue ArgumentError => e
      @upload.delete
      flash[:alert] = 'Error during importing sales, please check the file'
      render :new
    end
  end

  def show
    @upload = Upload.find(params[:id])
    @sales = @upload.sales.includes(:client, item_sales: [:item])
  end

  private

  def upload_params
    params.require(:upload).permit(:description, :file)
  end
end
