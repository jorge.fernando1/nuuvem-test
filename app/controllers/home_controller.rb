class HomeController < ApplicationController
  def index
    @last_uploads = current_user.uploads.order('created_at DESC').limit(5)
  end
end
