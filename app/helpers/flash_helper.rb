module FlashHelper
  def render_flash_messages
    return unless flash.any?
    [:notice, :alert].map { |type| render_alert(type) }.join.html_safe
  end

  private

  def render_alert(type)
    return unless flash[type]
    klass = find_alert_klass(type)

    content_tag(:div, class: "alert alert-#{klass}", role: 'alert') do
      content_tag(:span, flash[type])
    end
  end

  def find_alert_klass(type)
    case type
    when :notice then :success
    when :alert then :warning
    else type
    end
  end
end
