class Item < ApplicationRecord
  monetize :price_cents

  has_many :item_sales
  belongs_to :merchant
end
