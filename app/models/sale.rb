class Sale < ApplicationRecord
  has_many :item_sales
  has_many :items, through: :item_sales

  belongs_to :client
  belongs_to :upload
end
