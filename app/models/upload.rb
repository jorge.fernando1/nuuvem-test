class Upload < ApplicationRecord
  mount_uploader :file, FileUploader

  has_many :sales
  belongs_to :user


  # Usually I would use a decorator, but because this a test, I will just make the calc

  def total_gross
    total = Money.new(0)
    sales.each do |sale|
      values = sale.item_sales.map do |item_sale|
        item_sale.price * item_sale.quantity
      end
      total += values.sum
    end
    total
  end
end
