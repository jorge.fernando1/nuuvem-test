class Merchant < ApplicationRecord
  has_many :sales
  has_many :items
end
