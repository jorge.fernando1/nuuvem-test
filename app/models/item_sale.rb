class ItemSale < ApplicationRecord
  monetize :price_cents

  belongs_to :item
  belongs_to :sale
end
